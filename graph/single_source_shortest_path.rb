module ShortestPath
  def relax(edge)
    if edge.destination.sp_estimate > edge.source.sp_estimate + edge.distance
      edge.destination.sp_estimate = edge.source.sp_estimate + edge.distance
      edge.destination.parent = edge.source
    end
  end

  def extract_minimum(vertices)
    vertices.min_by(&:sp_estimate)
  end

  def adjoint_edges(edges, vertex)
    edges.select{ |e| e.source.name == vertex.name }
  end

  def dijkstra
    vertices = self.vertices
    result = []
    while !vertices.empty?
      vertex = extract_minimum(vertices)
      result << vertex
      vertices.delete(vertex)
      a = adjoint_edges(self.edges, vertex)
      a.each do |edge|
        relax(edge)
      end
    end
    result
  end
end
