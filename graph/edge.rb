require 'unitwise'

class Edge
  DEFAULT_DISTANCE_UNIT = 'km'

  def initialize(source, destination, distance, distance_unit)
    @source = source
    @destination = destination
    @distance = Unitwise(distance, distance_unit).convert_to(DEFAULT_DISTANCE_UNIT).to_f
  end

  attr_reader :source, :destination
  attr_accessor :distance

  def distance=(opts)
    Unitwise(opts[:value], opts[:unit]).convert_to(DEFAULT_DISTANCE_UNIT).to_f
  end
end
