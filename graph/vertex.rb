class Vertex
  def initialize(name, sp_estimate = Float::INFINITY, parent = nil)
    @name = name
    @sp_estimate = sp_estimate
    @parent = parent
  end

  attr_reader :name
  attr_accessor :sp_estimate, :parent

  def pretty_print_result
    route = []
    destination = self.dup
    route = [destination.name]
    while(!destination.parent.nil?) do
      route << destination.parent.name;
      destination = destination.parent;
    end
    {
      route: route.reverse.join('-'),
      distance: self.sp_estimate,
      unit: Edge::DEFAULT_DISTANCE_UNIT
    }
  end
end
