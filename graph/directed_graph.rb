require_relative './vertex'
require_relative './edge'
require_relative './single_source_shortest_path'
require 'json'

class DirectedGraph
  include ShortestPath

  def initialize(json_file)
    @vertices = []
    @edges = []
    edges = JSON.parse(json_file)
    edges['routes'].each do |route|
      source = @vertices.select{ |v|
        v.name == route['source']
      }.first || Vertex.new(route['source'])

      destination = @vertices.select{ |v|
        v.name == route['destination']
      }.first || Vertex.new(route['destination'])

      @vertices |= [source]
      @vertices |= [destination]

      if edge = edge_already_exists?(source, destination)
        edge.distance = { value: route['distance'], unit: route['distance_unit'] }
      else
        @edges << Edge.new(source, destination, route["distance"], route["distance_unit"])
      end

      unless edge_already_exists?(destination, source)
        @edges << Edge.new(destination, source, route["distance"], route["distance_unit"])
      end
    end
  end

  def edge_already_exists?(source, destination)
    @edges.select{ |e|
      e.source.name == source.name && e.destination.name == destination.name
    }.first
  end

  attr_reader :vertices, :edges, :source
  attr_reader :destination

  def source_intialize(source)
    @source = @vertices.select { |vertex| vertex.name == source }.first
    @source&.sp_estimate = 0
  end

  def find_shortest_route(destination)
    @result_vertices = dijkstra
    @destination = @result_vertices.select {
      |vertex| vertex.name == destination
    }.first || Vertex.new(destination)
  end
end
