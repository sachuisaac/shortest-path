# Shortest Route #

Computes the shortest route between two places. The seed values for the routes are stored in json format in `input_routes.json`. To compute the shortest path, Dijkstra's Algorithm has been used.

How to run the program ( uses ruby MRI 2.3.1 )

* `git clone https://sachuisaac@bitbucket.org/sachuisaac/shortest-path.git`

* `bundle install`

* `ruby app.rb "<SOURCE>" "<DESTINATION>"`

Example of output:
```
~/shortest-path$ ruby app.rb 'Mangalore' 'Chickmagalur'
{
       :route => "Mangalore-Mysore-Bangalore-Belgaum-Chickmagalur",
    :distance => 367.0,
        :unit => "km"
}
```

The distance unit is constant value in `Edge` class. Changing of which would convert to desirable units

Gem Dependencies:

* `awesome_print` --> for pretty representation of hashes

* `unitwise`      --> for unit converstions


How to run specs (Rspec)

* `rspec --color -I . -I ./spec ./spec/integration/shortest_path_spec.rb`