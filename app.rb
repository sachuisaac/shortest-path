require_relative './graph/directed_graph'
require 'awesome_print'

module ShortestPath
  unless ARGV.length == 2
    puts "enter the source and destination"
    exit
  end

  graph = DirectedGraph.new(File.read('./input_routes.json'))

  graph.source_intialize(ARGV[0])
  ap graph.find_shortest_route(ARGV[1])&.pretty_print_result
end
