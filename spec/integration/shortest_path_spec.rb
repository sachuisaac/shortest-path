require_relative '../spec_helper'

describe 'Shortest Path' do
  let(:input_json) {
    {
      routes: [
        {
          source: "A",
          destination: "B",
          distance: 1,
          distance_unit: "km"
        },
        {
          source: "B",
          destination: "C",
          distance: 2,
          distance_unit: "km"
        },
        {
          source: "C",
          destination: "D",
          distance: 2,
          distance_unit: "km"
        },
        {
          source: "D",
          destination: "A",
          distance: 3,
          distance_unit: "km"
        },
        {
          source: "E",
          destination: "F",
          distance: 3,
          distance_unit: "km"
        }
      ]
    }
  }
  let(:graph) { DirectedGraph.new(input_json.to_json) }

  describe 'creation of graph' do
    it 'returns a directed graph with all the edges and vertices' do
      expect(graph.vertices.size).to be(6)
      expect(graph.edges.size).to be(10)
    end
  end

  describe 'source intialize the graph' do
    context 'when source is valid' do
      it 'intialize the source node properties' do
        graph.source_intialize('A')
        source = graph.vertices.select{|v| v.name == 'A'}.first
        expect(source.sp_estimate).to be(0)
        expect(source.parent).to be_nil
      end
    end

    context 'when source is invalid' do
      it 'does not intialize the source node properties' do
        graph.source_intialize('Z')
        source = graph.vertices.select{|v| v.name == 'Z'}.first
        expect(source).to be_nil
      end
    end
  end

  describe 'computes the shortest path between two nodes' do
    context 'when source is valid' do
      before(:each) do
        graph.source_intialize('A')
      end

      context "when destination is valid and reachable" do
        context 'when destination and source are different' do
          it 'returns the valid result' do
            destination_node = graph.find_shortest_route('B')
            expect(destination_node.name).to eq('B')
            result = destination_node.pretty_print_result
            expect(result[:route]).to eq('A-B')
            expect(result[:distance]).to be(1.0)
            expect(result[:unit]).to eq('km')
          end

          it 'returns the shortest path' do
            destination_node = graph.find_shortest_route('D')
            expect(destination_node.name).to eq('D')
            result = destination_node.pretty_print_result
            expect(result[:route]).to eq('A-D')
            expect(result[:distance]).to be(3.0)
            expect(result[:unit]).to eq('km')
          end
        end

        context 'when destination and source are same' do
          it 'returns the valid result' do
            destination_node = graph.find_shortest_route('A')
            expect(destination_node.name).to eq('A')
            result = destination_node.pretty_print_result
            expect(result[:route]).to eq('A')
            expect(result[:distance]).to be(0)
            expect(result[:unit]).to eq('km')
          end
        end
      end

      context 'when destination is valid and unreachable' do
        it 'returns the valid result' do
          destination_node = graph.find_shortest_route('E')
          expect(destination_node.name).to eq('E')
          result = destination_node.pretty_print_result
          expect(result[:route]).to eq('E')
          expect(result[:distance]).to be(Float::INFINITY)
          expect(result[:unit]).to eq('km')
        end
      end

      context 'when destination is invalid and unreachable' do
        it 'returns the valid result' do
          destination_node = graph.find_shortest_route('Z')
          expect(destination_node.name).to eq('Z')
          result = destination_node.pretty_print_result
          expect(result[:route]).to eq('Z')
          expect(result[:distance]).to be(Float::INFINITY)
          expect(result[:unit]).to eq('km')
        end
      end
    end

    describe 'when the source is invalid' do
      it "returns a valid result" do
        graph.source_intialize('Z')
        destination_node  = graph.find_shortest_route('A')
        expect(destination_node.name).to eq('A')
        result = destination_node.pretty_print_result
        expect(result[:route]).to eq('A')
        expect(result[:distance]).to be(Float::INFINITY)
        expect(result[:unit]).to eq('km')
      end
    end
  end
end
